# Microlens grid estimation, light field decoding, and calibration

Source code accompanying the paper "Microlens array grid estimation, light field decoding, and calibration"
and our example Dataset "Synthetic Whiteimages for the Lytro Illum Light Field Camera with Ground Truth Microlens Center Coordinates".

If you use our dataset or source code, please cite:

    @Article{Schambach2020,
      author  = {Schambach, Maximilian and Puente León, Fernando},
      title   = {Microlens array grid estimation, light field decoding, and calibration},
      journal = {IEEE Transactions on Computational Imaging},
      volume  = {6},
      pages   = {591--603},
      year    = {2020},
    }
    
Our dataset is available at [IEEE DataPort](https://ieee-dataport.org/open-access/synthetic-whiteimages-lytro-illum-light-field-camera-ground-truth-microlens-center).

To synthesize and/or evaluate your own white image, follow these steps.


## Preparation

### Dependencies
You will need Python 3.6 or newer to run the provided code.
Furthermore, the code depends on our provided Python 
package [plenpy](https://gitlab.com/iiit-public/plenpy). You can intall it simply via PyPi using

    $ pip install plenpy
    
It is recommended to install the package to a new environment.



### Parameters

In the file ``support/support.py`` (line 18 - 29), insert your desired MLA 
parameters, including:

- the sensor size and pixel pitch
- the main lens size
- the microlens diameter
- the microlens array grid layout
- the number of samples used during ray tracing

If you use our Dataset (Lytro Illum camera), leave these values as is.

When you change your camera parameters, you might need to adapt the object side
aperture distances as specified in the ``add_vignetting()`` function.
The provided aperture parameters hold for the Lytro Illum camera and need to be hand-set for
every other camera.

Once set, in the following, leave those global variables unchanged.

To render the created white image scenes, use our provided [raytracer](https://gitlab.com/iiit-public/raytracer).
For details, please refer to its [official documentation](https://iiit-public.gitlab.io/raytracer/).

## 1. Scene creation
The programm ``01_create_scenes.py`` creates the JSON scene discription that 
serve as the input for the [raytracer](https://gitlab.com/iiit-public/raytracer).
You can pass options to the pogram via the command line, run

    $ python 01_create_scenes --help
    
to view the program help. Using the according options, you can set different
values for the MLA rotation, offset, main lens focal length, focus distance, etc.
If you run the program without any explicit options, the default values are used
which correspond to the values used in our paper.

The JSON files, together with some metadata, are saved to the ``Scenes`` folder
if not specified otherwise. Please, render all scenes using the raytracer.
Note that, depending on the number of samples set in the ``support.py`` file,
the rendering process might take a very long time, as the MLA ray tracing cannot
utilize multiprocessing. With the defaults values, rendering of a single image,
using one CPU core, takes about 2 days. It is adviced to render the images on a
parallel infrasctructure.

## 2. Grid estimation
The program ``02_estimate_grid.py`` uses the rendered white images, together
with the reference ML centers created by the raytracer, and estimates
a regular grid for each, using different methods. The results are saved
in a CSV file. Again, see

    $ python 02_estimate_grid.py --help
    
for help on the possible options. Usually, you can simply run it, pointing
to the folder containing all rendered images (as `.png`) together with their
metadata and ML centers (`.metadata` and `.csv` files), running

    $ python 02_estimate_grid.py --folder <path-to-wi-folder>
    
You can also simply add your own grid estimation method to the
``support/grid_estimation.py`` module and adding it to the evaluation script's
``method`` option.


## Evaluation

### Optional: Decoding
If you want to evaluate the decoding of the raw light field using the different
estimation algorithms, see the ``03_decode.py`` script. You have
to create and render your own scenes first. For examples, see the ``Example Scenes`` folder.
In the presented version it decodes both the ray traced as well raw images from a Lytro Illum camera
for comparison. 

### Performance evaluation
For the evaluation of the grid estimation performance, feel free to use our
Jupyter notebook ``04_evaluate.ipynb``. 
You will have to adapt some folder paths in the first blocks, but the file is commented and self explanatory.


## Additional remarks
For the estimates made in our paper, please refer to the ``accuracy_estimates.py``
script, as well as the ``geometry_helper.m`` MATLAB code.
You can again adapt the MLA parameters to check the estimates for you specific
MLA based application.