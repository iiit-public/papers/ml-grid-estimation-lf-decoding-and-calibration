% Evaluate center detection using the MATLAB source code provided by
% Y. Bok, see https://sites.google.com/site/yunsubok/lf_geo_calib
% and source code https://docs.google.com/open?authuser=earboll%40gmail.com&id=0B2553ggh3QTcRkY5OWRIaURESjA
% The scripts calculating the center positions are copyright by Y. Bok.

%% Add paths
addpath('./bok');

%%
%folderpath = "/mnt/lsdf-os/MLA Synthesis Data/Raytracer/";
%folderpath = "/home/ubuntu/data_mla/Raytracer/"
folderpath = "/home/schambach/Desktop/mla-test/";

output = "center_detection_measured_['bok']_" + string(date) + ".csv";

noise = [0, 0.001, 0.005, 0.01];
spacing = 14;
radius = 7;
rot = 0;

% Lytro parameters (raytracer implementation)
global SENSORSIZE;
SENSORSIZE = [7728, 5368];
global FNUM
FNUM = 2.0;
global FNUM_ML;
FNUM_ML = 0.975 * FNUM ;
global MICROLENSRADIUS;
MICROLENSRADIUS = 10e-6;
global PIXELPITCH;
PIXELPITCH = 1.4e-6;

% Read all PNG images in folder
files = [];

content = dir(folderpath);
file_list = {content(:).name};
for k = 1 : length(file_list)
    if endsWith(file_list{k}, '.png')
        files = [files;string(file_list{k})];
    end
end

% output CSV file columns
%cols = ['id', 'image_distance', 'offset', 'rotation', 'grid_noise', 'img_noise', 'vignetting', 'method', 'runtime', 'q_g', 'q_g_std', 'q_s', 'q_r'];
df = table();

% Iterate through all White Images
disp("Reading white image files...")
for k = 1 : length(files)    
    disp("Reading white image " + string(k) + " of " + string(length(files)) + ".")
    
    % Load image
    img_orig = imread(folderpath + files{k});
    
    for n = 1: length(noise)
        disp("Using noise " + string(n) + " of " + string(length(noise)) + ".")
        % WI preprocessing
        img = wi_preprocess(img_orig, noise(n));
        % Load metadata file
        filebase = files{k}(1:end-4);
        metadata_file = filebase + ".metadata";
        metadata = jsondecode(fileread(folderpath + metadata_file));

        raytracer_filename = metadata.raytracerJsonfile;
        id = string(raytracer_filename(end-12:end-5));
        offset = transpose(metadata.offset);
        offset = "["+string(offset(1))+","+string(offset(2))+"]";
        grid_noise = metadata.grid_noise;
        img_noise = noise(n);
        rotation = metadata.rotation;
        vignetting = metadata.vignetting;
        method = "bok";
        image_distance = metadata.imageDistance;
        focus_distance = metadata.focusDistance;
        lens_focal_length = 1.0 / (1.0 / image_distance + 1.0 / focus_distance);
        f_num = metadata.fNumber;
        f_num_ml = 0.975 * f_num;
        ml_radius = metadata.microlensRadius;
        ml_focal_length = f_num_ml * 2.0 * ml_radius;
        pixel_pitch = metadata.pixelPitch;

        % Load grid points ground truth csv file
        gridpoints_file = filebase + "_gridPoints.csv";
        centers = readmatrix(folderpath + gridpoints_file, 'NumHeaderLines',1);

        %Estimated magnification factor lambda
        lam_est = (image_distance + ml_focal_length) / image_distance;
        spacing = lam_est*2*ml_radius / pixel_pitch;

        centers_o = get_orthogonal_centers(centers);
        centers_p = get_projected_centers(centers, image_distance);

        % Get center estimates from BOK
        disp("Calculating centers...");
        tic;
        centers_bok = get_bok_centers(img);
        runtime = toc;
        
        % Calculate accuracy
        
        % Calculate KDTree for Ground Truth centers
        disp("Calculating KDTree...");
        gt_tree = KDTreeSearcher(centers_p);
        
        shape = size(centers_bok);
        num_centers = shape(1);

            
        disp("Calculating NN...");

        [idx_,d_] = knnsearch(gt_tree,centers_bok,'K',1);
        % sort out points that are too far away (misdetection)
        d_(d_ > 0.45*spacing) = nan;

        q_g = nanmean(d_);
        q_g_std = nanstd(d_);
        q_g_rmse = sqrt(nanmean(d_.^2));
        q_s = nan;
        q_r = nan;
        
        disp("...done. Qg: " + string(q_g) + "  pm  " + string(q_g_std) + " RMSE: " + string(q_g_rmse))
        
        % Append to table
        %cols = ['id', 'image_distance', 'offset', 'rotation', 'grid_noise', 
        %'img_noise', 'vignetting', 'method', 'runtime', 'q_g', 'q_g_std', 
        %'q_s', 'q_r'];
        
        df_tmp = table(id, image_distance, offset, rotation, grid_noise, ...
            img_noise, vignetting, method, runtime, q_g, q_g_std, q_g_rmse, q_s, q_r);
        df = [df; df_tmp];

        
        % DEBUG: show center points
    %     imshow(uint16(img));
    %     hold on;
    %     scatter(centers_p(:, 2), centers_p(:, 1), 15, 'filled');
    %     scatter(centers_o(:, 2), centers_o(:, 1), 15, 'x');
    %     pause
    %     hold off;

    end % end noise loop
    
% Write intermediate results to file
writetable(df, folderpath + output,'Delimiter',',')
end % end file loop

% Write final results to file
writetable(df, folderpath + output,'Delimiter',',')


%%
% Helper Function definitions

% Apply GBRG Bayer pattern to image
function img_mosaiced = mosaic(img)

    % Initialize RGB channels
    R = zeros(size(img(:, :, 1)));
    B = R;
    G = R;
    
    R(2:2:end, 1:2:end) = img(2:2:end, 1:2:end,1);
    G(1:2:end, 1:2:end) = img(1:2:end, 1:2:end, 2);
    G(2:2:end, 2:2:end) = img(2:2:end, 2:2:end, 2);
    B(1:2:end, 2:2:end) = img(1:2:end, 2:2:end,3);
    
    img_mosaiced = sum(cat(3, R, G, B), 3);
    
    
end

% White image preprocessing
function wi_processed = wi_preprocess(wi, noise)
    % Resample to 10 bit (stored in 16 bit uint) and convert to float value
    res = double(uint16((2^10 - 1) * (double(wi) ./ (2^16 - 1)))) ./ (2^10 - 1);
    
    % Gamma according to lytro metadata
    res = res.^0.4;

    % Apply RGB response according to Lytro's metadata
    res(:, :, 1) = res(:, :, 1) .* 0.7761;
    res(:, :, 2) = res(:, :, 2) .* 1.0;
    res(:, :, 3) = res(:, :, 3) .* 0.7345;

    % Mosaic
    res = mosaic(res);

    % Add noise
    if noise ~= 0
        
        noise_mean = 0;
        noise_sigma = noise;
        
        size_ = size(res);
        size_x = size_(1);
        size_y = size_(2);
        
        res = res + normrnd(noise_mean, noise_sigma, size_x, size_y);
    end


    % Contrast stretch
    res = res - min(min(res));
    tmp = res(2500:end-2500, 3500:end-3500);
    res =  res / max(max(tmp));
    
    res(res < 0) = 0.0;
    res(res > 1) = 1.0;

    % Gamma correct
    res = res.^(1 / 0.4);
    
    % Demosaic
    res = demosaic(uint16(res .* (2^16 - 1)), 'GBRG'); % or GRBG
    
        
    % Convert to Greyscale
    res = rgb2gray(res);
    
    % return result ranged 0, 2^16 -1 for BOK algorithm
    wi_processed = double(res);
end

% Calculate perspectively projected centers from camera coordinate centers
function centers_p = get_projected_centers(centers, imageDistance)
    global FNUM_ML MICROLENSRADIUS SENSORSIZE PIXELPITCH;
    
    centers_tmp = centers;
    
    %Micro lens focal length
    microLensFocalLength = FNUM_ML * 2.0 * MICROLENSRADIUS;

    %Scaling factor (central projection)
    sfac = (imageDistance + microLensFocalLength) / imageDistance;

    %project centers and convert to pixels
    centers_p = (sfac * centers_tmp(:, 1:2) ./ PIXELPITCH) + 0.5 * SENSORSIZE;

    %lip image coordinates (as is done in raytracer)
    centers_p = SENSORSIZE - centers_p;

    %Shift as pixel starts at -0.5, -0.5 ant not as 0, 0 as in raytracer
    centers_p = centers_p + [0.5, 0.5];

    centers_p = fliplr(centers_p);
end

% Calculate orthogonally projected centers from camera coordinate centers
function centers_o = get_orthogonal_centers(centers)
    global SENSORSIZE PIXELPITCH;
    
    centers_tmp = centers;

    %project centers and convert to pixels
    centers_o = (centers_tmp(:, 1:2) ./ PIXELPITCH) + 0.5 * SENSORSIZE;

    %flip image coordinates (as is done in raytracer)
    centers_o = SENSORSIZE - centers_o;

    %Shift as pixel starts at -0.5, -0.5 ant not as 0, 0 as in raytracer
    centers_o =  centers_o + [0.5, 0.5];

    centers_o = fliplr(centers_o);
end

% Calculate ML centers using method by Y. BOK
    % Copied from source code by Y. BOK (see top of file)
function centers_bok = get_bok_centers(img)
    radius=7;
    height=size(img,1);
    width=size(img,2);
    iteration_max=20;

    idxS=ceil((height/radius)*(width/radius)/2);

    center_list=zeros(2,idxS*2);

    center=MicroLensCenter(img,[width/2;height/2],radius,iteration_max*10);

    idxL=ceil(height/radius/2);
    idxR=idxL;
    center_line_init=zeros(2,idxL*2);
    center_line_init(:,idxL)=center;
    while true
        center=MicroLensCenter(img,center-[radius*2;0],radius,iteration_max);
        if center(1)>0
            idxL=idxL-1;
            center_line_init(:,idxL)=center;
        else
            break;
        end
    end
    center=center_line_init(:,idxR);
    while true
        center=MicroLensCenter(img,center+[radius*2;0],radius,iteration_max);
        if center(1)>0
            idxR=idxR+1;
            center_line_init(:,idxR)=center;
        else
            break;
        end
    end
    center_line_init=center_line_init(:,idxL:idxR);
    center_list(:,idxS:idxS+idxR-idxL)=center_line_init;
    idxE=idxS+idxR-idxL;

    triangle_direction=1;

    center_line=center_line_init;
    while true
        len=size(center_line,2);
        center_line=[center_line(:,1)-[radius*2;0],center_line,center_line(:,len)+[radius*2;0]];
        center_line=MicroLensCenter(img,center_line+repmat(radius*[triangle_direction;-1.73],1,size(center_line,2)),radius,iteration_max);
        center_line(:,center_line(1,:)<0)=[];
        if isempty(center_line)
            break;
        else
            center_list(:,idxS-size(center_line,2):idxS-1)=center_line;
            idxS=idxS-size(center_line,2);
            triangle_direction=-triangle_direction;
        end
    end

    center_line=center_line_init;
    while true
        len=size(center_line,2);
        center_line=[center_line(:,1)-[radius*2;0],center_line,center_line(:,len)+[radius*2;0]];
        center_line=MicroLensCenter(img,center_line+repmat(radius*[triangle_direction;1.73],1,size(center_line,2)),radius,iteration_max);
        center_line(:,center_line(1,:)<0)=[];
        if isempty(center_line)
            break;
        else
            center_list(:,idxE+1:idxE+size(center_line,2))=center_line;
            idxE=idxE+size(center_line,2);
            triangle_direction=-triangle_direction;
        end 
    end
    centers_bok = fliplr(transpose(center_list(:,idxS:idxE)));
end