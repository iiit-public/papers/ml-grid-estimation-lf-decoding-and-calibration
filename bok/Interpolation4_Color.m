% Code by Y. Bok. 
% https://sites.google.com/site/yunsubok/lf_geo_calib
% Copyright

function val=Interpolation4_Color(location,img)
i=floor(location);
s=location-i;
k=length(img(1,1,:));
val=zeros(k,length(location(1,:)));
img_size=size(img);
index=find(i(1,:)<1|i(1,:)>img_size(2)-1|i(2,:)<1|i(2,:)>img_size(1)-1);
i(:,index)=1;

% Optimization to speed up processing time
img_size(3) = img_size(1)*img_size(2);

for n=0:k-1    
%     temp=img(:,:,n+1);
%     i00=temp(sub2ind(size(temp),i(2,:),i(1,:)));
%     i01=temp(sub2ind(size(temp),i(2,:),i(1,:)+1));
%     i10=temp(sub2ind(size(temp),i(2,:)+1,i(1,:)));
%     i11=temp(sub2ind(size(temp),i(2,:)+1,i(1,:)+1));     
%     val(n+1,:)=(i00.*(1-s(1,:))+i01.*s(1,:)).*(1-s(2,:))+(i10.*(1-s(1,:))+i11.*s(1,:)).*s(2,:);
    
    % Optimizations to speed up processing time
    i00=img(i(2,:)   + img_size(1)*(i(1,:)-1) + img_size(3)*n);
    i01=img(i(2,:)   + img_size(1)* i(1,:)    + img_size(3)*n);
    i10=img(i(2,:)+1 + img_size(1)*(i(1,:)-1) + img_size(3)*n);
    i11=img(i(2,:)+1 + img_size(1)* i(1,:)    + img_size(3)*n);
    val(n+1,:) = ( i00 + (i01-i00).*s(1,:)).*(1-s(2,:)) ...
                 +(i10 + (i11-i10).*s(1,:)).*s(2,:);
end
val(:,index)=-1;