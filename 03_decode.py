# -*- coding: utf-8 -*-1.3
# Copyright (C) 2019  Maximilian Schambach
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import os
import numpy as np


##

from plenpy.cameras.raytracer_lightfield_camera import RayTracerLF
from plenpy.cameras.lytro_illum import LytroIllum

save_path = os.path.abspath("./")

path = os.path.abspath("./Evaluation/Raytracer")
path_lytro = os.path.abspath("./Evaluation/Lytro")

# Create cameras for Raytracer and Lytro images, using method by Dansereau
cam_dans = RayTracerLF(path)
cam_dans.calibrate(filename='cal_data_dans', method='dans')

cam_lytro_dans = LytroIllum(path_lytro)
cam_lytro_dans.calibrate(filename='cal_data_dans', method='dans')

# Create cameras for Raytracer and Lytro images, using proposed method
cam_prop = RayTracerLF(path)
cam_prop.calibrate(filename='cal_data_prop', method='own')

cam_lytro_prop = LytroIllum(path_lytro)
cam_lytro_prop.calibrate(filename='cal_data_prop', method='own')

# Decode Checker board images
# 30 mm Lens, focus at infinity
# Corresponding Lytro wi: MOD0033.RAW

for cam, type, name in zip([cam_dans, cam_lytro_dans, cam_prop, cam_lytro_prop],
                           ["RayTracer", "Lytro", "RayTracer", "Lytro"],
                           ["dans", "dans", "prop", "prop"]):

    cam.load_sensor_image(0)
    cam.decode_sensor_image(0)
    x = cam.get_decoded_image(0)
    x.save_rgb(save_path + f"lf_{type}_{name}.png")
