# -*- coding: utf-8 -*-1.3
# Copyright (C) 2019  Maximilian Schambach
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""Estimate regular grid from white image"""

import argparse
from datetime import date
import json
import logging as lg
import logging.handlers as lgh
import time
import os

import imageio
import numpy as np
import pandas as pd

from support import support as supp
from support import grid_estimation as ge


def parse_args():
    # Argument setup
    parser = argparse.ArgumentParser(
        description="Estimate regular grid from white image(s).")

    group = parser.add_mutually_exclusive_group(required=True)

    group.add_argument('-F', '--folder', metavar='PATH',
                       nargs='+',
                       help='Folder path of white images.')

    group.add_argument('-f', '--file', metavar='PATH',
                       nargs='+',
                       help='Path to single white image.')

    parser.add_argument('-m', '--method', type=str,
                        choices=['dans', 'cho', 'prop'],
                        default=['dans', 'cho', 'prop'],
                        nargs='+',
                        help='Method used for grid estimation.')

    parser.add_argument('-c', '--crop', type=int,
                        default=20,
                        help='Amount to be cropped from detected ml center borders')

    parser.add_argument('-s', '--spacingguess', type=float,
                        default=14,
                        help='A guess of the grid spacing. '
                             'Only used for dans grid estimate')

    parser.add_argument('-R', '--rotguess', type=float,
                        default=0,
                        help='A guess of the grid rotation in radian. '
                             'Only used for dans grid estimate')

    parser.add_argument('-o', '--output', type=str,
                        help='Output folder path.')

    parser.add_argument('-S', '--show', action='store_true',
                        help='Show white image and estimated grid with actual ML centers.')

    parser.add_argument('-n', '--noise', type=float,
                        default=[0, 0.001, 0.005, 0.01],
                        nargs='+',
                        help='Standard deviation of Gaussian image noise to add.')

    return parser.parse_args()


if __name__ == "__main__":

    args = parse_args()

    # Extract arguments
    folderpath = args.folder
    imagepath = args.file
    methods = args.method
    noises = args.noise
    crop = args.crop
    spacing_guess = args.spacingguess
    rot_guess = args.rotguess
    output = args.output
    show = args.show

    if folderpath is None:
        folderpath = os.path.abspath("./")
    else:
        folderpath = os.path.abspath(folderpath[0])

    if output is None:
        folder_out = folderpath
    else:
        folder_out = output

    # Get output CSV name
    csv_filename = os.path.join(
            folder_out,
            f"center_detection_measured_{methods}_{date.today().isoformat()}.csv")

    # Create subfolder if non existent
    if not os.path.exists(os.path.dirname(csv_filename)):
        os.makedirs(os.path.dirname(csv_filename))

    counter = 0
    csv_basename = os.path.splitext(csv_filename)[0]
    while os.path.exists(csv_filename):
        csv_filename = csv_basename + f"_{counter:04d}.csv"
        counter += 1

    # Logger set up
    logger = lg.getLogger("estimate_grid")
    logger.setLevel(lg.INFO)

    handler = lgh.RotatingFileHandler(
        os.path.join(
            folder_out,
            f"./estimate_grid_{methods}_{date.today().isoformat()}.log"),
        maxBytes=(1048576*5), backupCount=7)
    formatter = lg.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # Read images
    files = []

    if imagepath is None:
        logger.info(f"Reading white images from folder {folderpath}")
        for file in sorted(os.listdir(folderpath)):
            # Check that the file is a readable image file
            if file.lower().endswith("png"):
                files.append(os.path.join(folderpath, file))

    else:
        logger.info(f"Reading white image files {imagepath}")
        files = imagepath

    # Create pandas data frame to save results
    cols = ['id', 'image_distance', 'offset', 'rotation', 'grid_noise', 'img_noise', 'vignetting', 'method',
            'runtime', 'q_g', 'q_g_std', 'q_g_rmse', 'q_s', 'q_r']
    df = pd.DataFrame([], columns=cols)

    curr_file = 0
    for file in files:
        curr_file += 1
        logger.info(f"Processing image {curr_file} of {len(files)}.")
        logger.info(f"File: {file}")

        # Get file without extension
        filebase = os.path.splitext(file)[0]

        # Load metadata
        with open(filebase + ".metadata") as json_file:
            metadata = json.load(json_file)

        # Expand metadata values
        res = supp.expand_metadata(metadata)

        id, sensor_size, pixel_pitch, image_distance, focus_distance, \
        lens_radius, ml_radius, lens_focal_length, ml_focal_length, \
        f_num, f_num_ml, grid_type, rotation, offset, grid_noise, vignetting = res

        # Rotation orientation is opposite here
        if not rotation == 0:
            rotation *= -1

        # Estimated magnification factor lambda
        lam_est = (image_distance + ml_focal_length) / image_distance

        # Perspectively projected grid spacing in px
        spacing = lam_est*2*ml_radius / pixel_pitch

        if grid_type.lower() == "hex":
            # Calculate full Hex grid spacing
            spacing = np.asarray([np.sqrt(3)*spacing, spacing])
        elif grid_type.lower() == "rect":
            # Calculate full Rect grid spacing
            spacing = np.asarray([spacing, spacing])

        # Load ground truth ml centers
        centers = np.loadtxt(filebase + "_gridPoints.csv", delimiter='; ')

        # Project centers and convert to pixels
        centers_p = supp.get_projected_centers(centers, image_distance)
        centers_o = supp.get_orthogonal_centers(centers, image_distance)

        # Read image
        white_image_orig = imageio.imread(file, format="PNG-FI")

        # Evaluate using the different methods and noise levels
        for noise in noises:
            logger.info(f"\nUsing image noise sigma '{noise}'.")

            # White image preprocess
            white_image = supp.wi_preprocess(white_image_orig, noise)

            for method in methods:
                logger.info(f"\nEstimating grid using method '{method}'.")

                # Measure detection runtime
                runtime = time.time()

                # Estimate grid
                grid_est = ge.get_grid_est(method=method, grid_type=grid_type, im=white_image,
                                           args=dict(lam_est=lam_est, spacing_guess=spacing_guess, rot_guess=rot_guess))

                runtime = time.time() - runtime

                # Save grid for decoding
                if noise == 0:
                    # Get filename without folder
                    f_name = os.path.split(filebase)[1]
                    f_name = os.path.join(folder_out, f_name)

                    if type(grid_est) is float and np.isnan(grid_est):
                        np.savez(f_name + f"_grid_est_{method}.npz", nan=grid_est)
                    else:
                        grid_est.save(f_name + f"_grid_est_{method}")

                # Show result if wanted
                if show:
                    ge.show(wi=white_image, centers_gt=centers_p, centers_est=grid_est.gridPoints)

                # Measure accuracy
                q_g, q_g_std, q_g_rmse, q_s, q_r = ge.get_accuracy(grid_est, centers_p, spacing, rotation)

                # Add new data to DataFrame
                df = df.append(
                    pd.DataFrame([[id, image_distance, offset, rotation, grid_noise, noise, vignetting, method, runtime, q_g, q_g_std, q_g_rmse, q_s, q_r]],
                                 columns=cols))

                # Redundant save in case error occurs
                df.to_csv(csv_filename, index=False)

    # Save total
    df.to_csv(csv_filename, index=False)
