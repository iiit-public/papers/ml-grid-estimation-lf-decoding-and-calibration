# -*- coding: utf-8 -*-1.3
# Copyright (C) 2019  Maximilian Schambach
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Support functions for grid parameter estimation"""

from typing import Tuple, Union, Optional
import logging as lg

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core import ndarray

from scipy import ndimage
from scipy.spatial import cKDTree
from scipy.optimize import differential_evolution
from skimage.feature import peak_local_max
from skimage import exposure
from skimage.color import rgb2gray
from scipy.ndimage.measurements import center_of_mass

from plenpy.utilities import kernels, grids, misc, images, demosaic

from support.ml_center_detection import get_ml_centers_dans, get_ml_centers_prop, get_ml_centers_cho, crop_ml_centers

# Logger set up
logger = lg.getLogger("estimate_grid")


def get_grid_est(method: str,
                 grid_type: str,
                 im: ndarray,
                 args: Optional[dict] = None) -> Union[grids.HexGrid, grids.RectGrid]:
    """Estimate a regular grid from a white image using the specified method.

    Args:
        method: Method used for estimation. One of "prop", "dans", "cho"

        grid_type: Grid layout type. Either "hex" or "rect".

        im: White image used for estimation in range [0, 1]

        args: Dictionary of arguments passed to the respective method's functions.

    Returns:
        Regular grid (HexGrid or RectGrid) estimated from the white image.

    """
    if args is None:
        args = {}

    # Get grid type
    # Calculate grid
    if grid_type.lower() == "hex":
        GridClass = grids.HexGrid

    elif grid_type.lower() == "rect":
        GridClass = grids.RectGrid

    else:
        raise ValueError(f"Unknown grid type {grid_type}.")

    # Use proposed method
    if method.lower() == "prop":
        # Extract options
        try:
            lam_est = args['lam_est']
        except KeyError:
            raise KeyError("Method 'prop' needs argument 'lam_est'.")

        if grid_type.lower() == "hex":
            logger.info("Estimating spacing and rotation...")
            res = est_grid_params_prop_hex(wi=im)
            spacing_est, spacing_std, rotation_est, rotation_std = res
            logger.info("...done")

            # Calculate missing grid spacing in x-direction
            # Here, grid implementation is such that y-spacing twice as large
            # as in paper
            spacing_est = np.asarray([np.sqrt(3)*spacing_est, spacing_est])

            logger.info("Estimating offset...")
            offset_est = est_grid_offset_prop(
                grid_type="hex", wi=im, lam_est=lam_est,
                spacing_est=spacing_est, rotation_est=rotation_est)
            logger.info("...done")

        elif grid_type.lower() == "rect":
            raise NotImplementedError()

    # Use method by D. Dansereau et al.
    elif method.lower() == "dans":
        # Extract options
        try:
            rot_guess = args['rot_guess']
            spacing_guess = args['spacing_guess']
        except KeyError:
            raise KeyError("Method 'dans' needs argument 'spacing_guess' and 'rotation_guess'.")

        if grid_type.lower() == "hex":
            # Estimate spacing and rotation
            logger.info("Estimating spacing and rotation...")
            spacing_est, rotation_est, ml_centers = est_grid_params_dans_hex(
                wi=im, spacing_guess=spacing_guess, rot_guess=rot_guess)
            logger.info("...done")

            # Estimate offset
            logger.info("Estimating offset...")
            offset_est = est_grid_offset_dans(
                grid_type="hex", wi=im, spacing_est=spacing_est, rotation_est=rotation_est, ml_centers=ml_centers)
            logger.info("...done")

        elif grid_type.lower() == "rect":
            raise NotImplementedError()

    # Use method by D. Cho et al.
    elif method.lower() == "cho":
        # Extract options
        try:
            rot_guess = args['rot_guess']
            spacing_guess = args['spacing_guess']
        except KeyError:
            raise KeyError("Method 'cho' needs argument 'spacing_guess' and 'rotation_guess'.")

        if grid_type.lower() == "hex":
            # Estimate spacing and rotation
            logger.info("Estimating spacing and rotation...")
            spacing_est, rotation_est, ml_centers = est_grid_params_cho_hex(
                wi=im, spacing_guess=spacing_guess, rot_guess=rot_guess)
            logger.info("...done")

            # Estimate offset
            logger.info("Estimating offset...")
            offset_est = est_grid_offset_cho(
                grid_type="hex", wi=im, spacing_est=spacing_est,
                rotation_est=rotation_est, ml_centers=ml_centers)
            logger.info("...done")

        elif grid_type.lower() == "rect":
            raise NotImplementedError()

    else:
        raise ValueError(f"Unknown method {method}. Must be either "
                         f"'prop', 'dans' or 'cho'.")

    # Log Results
    logger.info(f"Spacing est.:\t{spacing_est}")
    logger.info(f"Rotation est.:\t{rotation_est}")
    logger.info(f"Offset est.:\t{offset_est}")

    if np.any(np.isnan(spacing_est)) or np.any(np.isnan(rotation_est)) or np.any(np.isnan(offset_est)):
        return np.nan

    # Init grid parameters
    parameters_est = grids.GridParameters(
        size=im.shape, spacing=spacing_est, rotation=rotation_est, offset=offset_est)

    # Return final estimated grid
    return GridClass(parameters_est)


def est_offset(grid_type: str,
               ml_centers: ndarray,
               size: ndarray,
               spacing_est: ndarray,
               rotation_est:float,
               offset_init: Optional[ndarray] = None,
               window: Optional[ndarray] = None):
    """Estimate a grid offset

    Args:
        grid_type:
        ml_centers:
        size:
        spacing_est:
        rotation_est:
        offset_init:
        window:

    Returns:

    """

    if type(rotation_est) == np.nan or np.any(np.isnan(spacing_est)):
        return np.nan

    if not np.any(ml_centers):
        return np.nan

    if window is None:
        window = np.ones(size, dtype=np.uint8)

    GridClass = grids.HexGrid if grid_type == 'hex' else grids.RectGrid

    # Bild KDTree of detected centers
    ml_tree = cKDTree(ml_centers)

    # Initialize grid with estimated spacing as offset
    if offset_init is None:
        offset_init = np.asarray([spacing_est[1], spacing_est[1]])

    par_est = grids.GridParameters(size=size, offset=offset_init,
                                   spacing=spacing_est, rotation=rotation_est)

    grid_est = GridClass(par_est)

    # grid_est.crop(crop)
    grid_points = grid_est.gridPoints

    # Find offset of grid to ml center points
    offset = []
    for point in grid_points:
        _d, _idx = ml_tree.query(point)

        if _d < spacing_est[1]:
            weight = window[int(ml_centers[_idx][0]),
                            int(ml_centers[_idx][1])]

            offset.append(weight*(ml_centers[_idx] - point))

    return offset_init + np.median(np.asarray(offset), axis=0)


def get_accuracy(grid_est: Union[grids.HexGrid, grids.RectGrid],
                 centers_p: ndarray,
                 spacing_gt: ndarray,
                 rotation_gt: float) -> Tuple[float, float, float, float, float]:
    """Calculate the grid estimation accuracy using the ground truth grid points.

    Args:
        grid_est: Estimated grid.

        centers_p: Perspectively projected ground truth grid centers.

        spacing_gt: Ground truth spacing.

        rotation_gt: Ground truth rotation.

    Returns:
        ``q_g``, ``q_g_std``, ``q_g_rmse`` ,``q_s``, ``q_r``
        Grid accuracy q_g, spacing accuracy q_s, root mean square error
        q_g_rmse and rotation accuracy q_r.

        Overall accuracy ``q_g`` as mean absolute difference of grid points
        (in px) and the corresponding standard deviation ``q_g_std`` (in px),
        as well as the accuracy of the estimated grid spacing ``q_s``
        as mean absolute difference of the spacing vectors (in px) and the
        accuracy of the estimated rotation ``q_r`` as the absolute difference
        of rotation angles (in degrees).

    """

    # If grid is nan, return nan (might occur when estimation fails)
    if type(grid_est) == float and np.isnan(grid_est):
        return np.nan, np.nan, np.nan, np.nan, np.nan

    # Compare spacing and rotation
    logger.info("Calculating spacing and rotation accuracy...")
    spacing_est = grid_est.gridParameters.spacing
    rot_est = grid_est.gridParameters.rotation

    # Calculate differences
    q_r = float(np.rad2deg(np.abs(rotation_gt - rot_est)))
    q_s = float(np.linalg.norm(spacing_gt[1] - spacing_est[1]))
    logger.info("...done")

    # Calc accuracy of estimated grid vs ground truth
    logger.info("Calculating overall grid accuracy...")
    gt_tree = cKDTree(centers_p)

    # Find nearest neighbour for all estimated gridPoints to ground truth points
    _d, _idx = gt_tree.query(grid_est.gridPoints)
    diff = _d
    # Sort out the ones that are too large (failed detection)
    diff[diff > 0.45 * spacing_gt[1]] = np.nan

    diff = np.asarray(diff)
    q_g = float(np.nanmean(diff))
    q_g_std = float(np.nanstd(diff))
    q_g_rmse = float(np.sqrt(np.nanmean(diff**2)))
    logger.info("...done")

    logger.info(f"Estimation accuracies: q_g: {q_g} pm {q_g_std}, q_g_rmse: {q_g_rmse}, q_s: {q_s}, q_r: {q_r}")

    return q_g, q_g_std, q_g_rmse, q_s, q_r


def get_fourier_peaks(wi_fft: np.ndarray,
                      constrained: bool,
                      exponent: float = 2.0) -> ndarray:
    """Find peaks in absolute value of a Fourier transform of an image.

    Args:
        wi_fft: Input, FFT of an image. Should be of uneven shape, so that
                conversion to frequency units 1/px is correct.

        constrained: Whether to constrain the number of returned peaks.

        exponent: Exponent of the center of mass calculation around each peak.

    Returns:
        Peak coordinates (p_x, p_y) as Numpy array of shape
        (N, 2) in units 1/px.

    """

    size_x_fft, size_y_fft = wi_fft.shape

    # check that size is odd
    if size_x_fft % 2 == 0 or size_y_fft % 2 == 0:
        raise ValueError(f"Shape of FFT must be uneven! "
                         f"Found {size_x_fft, size_y_fft}.")

    # Find local maxima and values of FFT
    maxima = peak_local_max(wi_fft, min_distance=20)
    max_val = wi_fft[maxima[:, 0], maxima[:, 1]]

    # Get largest maxima values, constrain to largest 100 if desired
    if constrained:
        try:
            maxima = maxima[np.argsort(max_val)][-100:]
        except IndexError:
            maxima = maxima[np.argsort(max_val)]

    else:
        maxima = maxima[np.argsort(max_val)]

    # Get maxima in frequency units 1/px
    max_x = (maxima[:, 0] / (size_x_fft - 1)) - 0.5
    max_y = (maxima[:, 1] / (size_y_fft - 1)) - 0.5

    # Sort maxima by frequency distance, and use only first N components
    # excluding first peak which is the (0, 0) component
    if constrained:
        try:
            max_sort_idx = np.argsort(max_x ** 2 + max_y ** 2)[1:100]

        except IndexError:
            max_sort_idx = np.argsort(max_x ** 2 + max_y ** 2)[1:]
    else:
        max_sort_idx = np.argsort(max_x ** 2 + max_y ** 2)

    peak_coords = []

    # Calculate CoM around every maximum to get subpixel precision
    # Convert dtype to double precision avoid overflow due to large exponents
    com_pad = 7
    for idx, idy in maxima[max_sort_idx]:
        x, y = center_of_mass(
            wi_fft[idx - com_pad:idx + com_pad + 1,
                   idy - com_pad:idy + com_pad + 1].astype(np.float64) ** exponent
        ) - np.asarray([com_pad, com_pad])

        peak_coords.append([x + idx, y + idy])

    peak_coords = np.asarray(peak_coords) / np.asarray([size_x_fft - 1, size_y_fft - 1])

    # Shift zero freq
    peak_coords -= 0.5

    return peak_coords


def calc_grid_params_helper(x, im, f_mat, hires, show) -> Union[Tuple[float, float, float, float], float]:
    """Helper function to calculate spacing and rotation from a white image
    using different hyperparameters.

    Args:
        x: Hyperparameter vector. x = [x_q, x_gamma, x_exponent], where
            - q is pixel value lower bound used for contrast stretching
            - gamma is gamma correction factor used for value stretching
            - exponent is the exponent used in the center of mass calculation
            Note that x is normalized in [0, 1]^3

        im: Input white image.

        f_mat: Pre-calculated basis frequency vectors.

        hires: Whether to use hires FFT (zero padding) and return full grid values.

        show: Whether to show intermediate results for debugging.

    Returns:
        spacing_std [if hires == False] or
        Tuple (spacing, spacing_std, rot, rot_std) [if hires == True]

    """
    size = min(im.shape)

    if hires is True:
        pad = 1500
        window = "hann_rotational"
        n_max = 3
    else:
        pad = None
        window = "hann_rotational"
        n_max = 4

    # Map from unit interval to parameters interval
    q = 3 * 10**(1*x[0] - 2)        # range [5*10E-2, 5*10E-1]
    gamma = 10**(2*x[1] - 4)        # range [10E-4, 10E-2]
    sigma = (0.3 * x[2] + 0.3)      # range [0.3, 0.6]
    exponent = 2.5

    # Get rotationally symmetric Gauss kernel
    gauss = kernels.get_kernel_gauss(im.shape[0], sigma*size, im.shape[1], normalize='peak').astype(im.dtype)

    im_loc = gauss*(np.asarray(exposure.rescale_intensity(im, in_range=(q, 0.95)))**gamma).astype(im.dtype)
    del gauss

    wi_fft = np.abs(
        images.fourier(im_loc, shift=True, window=window, pad=pad,
                       implementation='numpy'))

    peak_coords = get_fourier_peaks(wi_fft, constrained=False, exponent=exponent)

    # create axis for more measurements
    f = f_mat[..., np.newaxis].copy()

    # Build KDTree of peaks
    peak_tree = cKDTree(peak_coords)

    # Now find the two peaks that correspond to
    # N*f_a and N*f_b for N=1, 2, 3,...
    points_detected = []
    for i in range(1, n_max + 1):

        idx_multi_f = []
        for f_tmp in f_mat:
            dist, idx = peak_tree.query(i * f_tmp, k=1)
            if dist < 0.005:
                idx_multi_f.append(idx)

        # have to have found two points for two basis vectors
        if len(idx_multi_f) == 2:
            tmp = (f, peak_coords[idx_multi_f, :, np.newaxis])
            f = np.concatenate(tmp, axis=-1)

            for idx in idx_multi_f:
                points_detected.append(peak_coords[idx])

    points_detected = np.asarray(points_detected)

    # delete first row, as they contain the "old" measurement
    f = f[..., 1:]

    # calculate mean distance of Nf and (N-1)f
    d_list_1 = [np.linalg.norm(f[0, :, 0])]
    d_list_2 = [np.linalg.norm(f[1, :, 0])]

    for i in range(1, f.shape[-1]):

        d_list_1.append(np.linalg.norm(f[0, :, i] - f[0, :, i-1]))
        d_list_2.append(np.linalg.norm(f[1, :, i] - f[1, :, i - 1]))

    d_list_tot = d_list_1 + d_list_2
    spacing_tot = np.mean(2 / (np.sqrt(3) * np.asarray(d_list_tot)))
    spacing_tot_std = np.std(2 / (np.sqrt(3) * np.asarray(d_list_tot)))

    spacing_1 = np.mean(2 / (np.sqrt(3) * np.asarray(d_list_1)))
    spacing_1_std = np.std(2 / (np.sqrt(3) * np.asarray(d_list_1)))

    spacing_2 = np.mean(2 / (np.sqrt(3) * np.asarray(d_list_2)))
    spacing_2_std = np.std(2 / (np.sqrt(3) * np.asarray(d_list_2)))

    # Choose measurement with smallest standard deviation
    spacing_std_min = np.min(np.asarray([spacing_1_std, spacing_2_std, spacing_tot_std]))

    if spacing_1_std == spacing_std_min:
        spacing = spacing_1
        spacing_std = spacing_1_std

    elif spacing_2_std == spacing_std_min:
        spacing = spacing_2
        spacing_std = spacing_2_std

    elif spacing_tot_std == spacing_std_min:
        spacing = spacing_tot
        spacing_std = spacing_tot_std

    else:
        raise ValueError("Something went wrong")

    # calculate rotation by line fit (swap x, y for numerical stability)
    fit1 = np.polyfit(f[1, 0, :], f[1, 1, :], deg=1, full=True)
    fit2 = np.polyfit(f[0, 0, :], f[0, 1, :], deg=1, full=True)

    alpha = np.arctan(fit1[0][0])
    try:
        alpha_std = fit1[1][0]
    except IndexError:
        alpha_std = 0

    # Show result for debug
    if show:
        # Plot detected points and estimated frequencies
        shift_x, shift_y = 1 / (2 * np.asarray(wi_fft.shape))

        extent = [-0.5 - shift_y, 0.5 + shift_y,
                  0.5 + shift_x, -0.5 - shift_x]

        fig, ax = plt.subplots()
        im_ = ax.imshow(wi_fft / wi_fft.size, aspect='equal', cmap='jet',
                       norm=LogNorm(vmin=1e-6, vmax=None), extent=extent)

        # ax.plot(peak_coords[:, 1], peak_coords[:, 0], 'o', color='pink',
        #         label='detected maxima', zorder=995)
        ax.plot(points_detected[:, 1], points_detected[:, 0], 'o', color='pink',
                label='used maxima', zorder=998)
        ax.quiver(f_mat[0, 1], f_mat[0, 0], color='red',
                  angles='xy', scale_units='xy', scale=1, label='ref', zorder=997)
        ax.quiver(f_mat[1, 1], f_mat[1, 0], color='red',
                  angles='xy', scale_units='xy', scale=1, label='ref', zorder=997)

        fig.legend()
        fig.colorbar(im_)
        plt.show()

        plt.figure()
        plt.imshow(im_loc)
        plt.show()

    if hires:
        logger.info(f"q: {q}, gamma: {gamma}, exponent: {exponent}, sigma: {sigma}.")
        return spacing, spacing_std, alpha, alpha_std
    else:
        return float(spacing_tot_std)


def est_grid_params_prop_hex(wi: ndarray, show: bool = False) -> Tuple[float, float, float, float]:
    """Estimate the grid parameters of a hex grid in the Fourier domain.

    This algorithm follows the presentation in [PAPER].
    The white image is fourier transformed and peaks are detected
    corresponding to the frequencies of the spatial basis vectors.
    From these, spacing and rotation are estimtaed.

    Returns:
        spacing, alpha
        Spacing vector and rotation of the grid in radians.

    """

    im = wi.copy()
    n, m = im.shape

    # crop wi to uneven shape so that there is a central pixel
    if n % 2 == 0:
        im = im[:-1, :]
    if m % 2 == 0:
        im = im[:, :-1]
    n, m = im.shape

    # crop to square shape (window will crop it anyways)
    s = min(n, m)
    im_small = im[(n-s)//2:s + (n-s)//2, (m-s)//2:s + (m-s)//2]
    n, m = im_small.shape

    # First, calculate an estimate of the frequency vectors
    wi_fft = np.abs(images.fourier(im, shift=True, window='hann_rotational',
                                   pad=1500, implementation='numpy'))

    peak_coords = get_fourier_peaks(wi_fft, constrained=True)

    # Find main frequency grid vectors
    # Only use peaks with positive f_x component and sort by descending f_y
    f_mat = peak_coords[0:6].copy()
    f_mat = f_mat[f_mat[:, 0] > 0, :]
    sort = np.argsort(f_mat[:, 1])
    f_mat = np.flipud(f_mat[sort, :])[0:2]

    # Now, optimize free parameters using differential evolution
    # Start with small population and iterations
    # if result is not good enough, increase size
    spacing_std = 1000
    counter = 0
    max_iter = 8
    pop_size = 3

    while spacing_std > 0.0015 and counter < 5:
        logger.info("Optimizing hyperparameters...")
        logger.info(f"Maxiter: {max_iter}, Popsize: {pop_size}")
        res = differential_evolution(calc_grid_params_helper,
                                     bounds=[(0, 1), (0, 1), (0, 1)],
                                     args=(im_small, f_mat, False, False),
                                     maxiter=max_iter,
                                     popsize=pop_size,
                                     mutation=(0.6, 1),
                                     recombination=0.6,
                                     atol=0.0025,
                                     tol=0.01,
                                     polish=False,
                                     updating='deferred',
                                     workers=-1)
        q_est, gamma_est, sigma_est = res['x']
        logger.info("...done")

        # calculate hi res result
        logger.info("Calculate HIRES result...")
        spacing, spacing_std, alpha, alpha_std = calc_grid_params_helper(
            np.asarray([q_est, gamma_est, sigma_est]), im, f_mat,
            hires=True, show=show)
        logger.info("...done")

        counter += 1
        max_iter += 5
        pop_size += 5
        # end while

    return spacing, spacing_std, alpha, alpha_std


def est_grid_offset_prop(wi: ndarray,
                         lam_est: float,
                         spacing_est: ndarray,
                         rotation_est: float,
                         grid_type: str,
                         show: bool = False) -> float:
    """Estimate overall grid offset.

    Args:
        wi: White image

        lam_est: Estimated magnification factor lam = (F +f) / F

        show: Whether to show results for debugging.

    Returns:
        Estimated grid offset

    """
    # Get ml centers and a window in central image region used for cropping/weighting
    ml_centers, window = get_ml_centers_prop(wi=wi, lam_est=lam_est)

    # Get first, rough estimate without weighting the central region
    offset_init = est_offset(grid_type=grid_type, ml_centers=ml_centers, size=wi.shape,
                      spacing_est=spacing_est, rotation_est=rotation_est)

    # Refine offset using higher weights in central region
    offset_refined = est_offset(grid_type=grid_type, ml_centers=ml_centers, size=wi.shape,
                      spacing_est=spacing_est, rotation_est=rotation_est, offset_init=offset_init, window=window)

    return offset_refined


def est_grid_params_dans_hex(wi: ndarray, spacing_guess: float, rot_guess: float):

    # Preprocess WI
    if spacing_guess == 15 or spacing_guess == 14:
        # use Matlab disk kernel to be directly comparable to toolbox
        conv_kernel = np.array([[0.        , 0.        , 0.        , 0.09814366, 0.39010312, 0.49165412, 0.39010312, 0.09814366, 0.        , 0.        , 0.        ],
                                [0.        , 0.00251692, 0.48357511, 0.97356963, 1.        , 1.        , 1.        , 0.97356963, 0.48357511, 0.00251692, 0.        ],
                                [0.        , 0.48357511, 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 0.48357511, 0.        ],
                                [0.09814366, 0.97356963, 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 0.97356963, 0.09814366],
                                [0.39010312, 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 0.39010312],
                                [0.49165412, 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 0.49165412],
                                [0.39010312, 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 0.39010312],
                                [0.09814366, 0.97356963, 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 0.97356963, 0.09814366],
                                [0.        , 0.48357511, 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 1.        , 0.48357511, 0.        ],
                                [0.        , 0.00251692, 0.48357511, 0.97356963, 1.        , 1.        , 1.        , 0.97356963, 0.48357511, 0.00251692, 0.        ],
                                [0.        , 0.        , 0.        , 0.09814366, 0.39010312, 0.49165412, 0.39010312, 0.09814366, 0.        , 0.        , 0.        ]])

    else:
        conv_kernel = kernels.get_kernel(name='disk',
                                         size=int(spacing_guess/3))

    # Convolve WI
    wi_conv = ndimage.filters.convolve(wi, conv_kernel)

    # Calculate ML centers
    ml_centers = get_ml_centers_dans(wi=wi_conv)

    # Crop ML centers
    ml_centers = crop_ml_centers(ml_centers=ml_centers,
                                 size=wi.shape,
                                 crop=int(10*spacing_guess))

    # Calculate spacing and rotation using the ML centers
    spacing_est, rotation_est = get_grid_params_dans_hex(
        ml_centers=ml_centers, spacing_guess=spacing_guess, rot_guess=rot_guess, wi=wi)

    return spacing_est, rotation_est, ml_centers


def get_grid_params_dans_hex(ml_centers: ndarray,
                             spacing_guess: float,
                             rot_guess: float,
                             wi: ndarray,
                             show: bool = False) -> Tuple[ndarray, float]:
    """Grid parameter estimation as implemented by Dansereau in the
    MATLAB Light Field Toolbox v0.4.

    The grid is traversed horizontally and vertically, performing line fits
    to estimate the grid rotation. For this a prior spacing and rotation
    estimate is necessary.

    Spacing is calculated as the mean distance of grid neighbors in the
    corresponding grid direction.

    Args:
        ml_centers: Estimated micro lens centers to be used for grid estimate.

        spacing_guess: A prior estimation of the grid spacing in y-direction.

        rot_guess: A prior estimation of the grid rotation in radians.

        show: Boolean flag indicating whether to plot results for debugging.

        white_img: White image. Only used for plotting.

    Returns:
        Estimated spacing (2D array) and rotation (in radians) of the grid.
        If not estimate is possible, returns nans.

    """

    # If no centers have been found, return nans
    if not np.any(ml_centers):
        return np.asarray([np.nan, np.nan]), np.nan

    crop = int(10*spacing_guess)

    # Build a KDTree for nearest neighbor search
    ml_tree = cKDTree(ml_centers)

    # Build estimated grid vectors (for hex grid)
    # First, for zero rotaion
    a = np.asarray([0, spacing_guess])
    b = np.asarray([spacing_guess*0.5*np.sqrt(3), 0.5*spacing_guess])

    # Rotate by estimated rotation
    if not rot_guess == 0:
        rot_mat = misc.rot_matrix(0, unit='radians')
        a = rot_mat @ a
        b = rot_mat @ b

    # Get conservative estimate of number of MLs per dimension
    num_x = int(wi.shape[0] / b[0]) + 100
    num_y = int(wi.shape[1] / a[1]) + 100

    # Find first lenslet as starting point
    _d, _idx = ml_tree.query(np.asarray([crop, crop]))
    ml_start = ml_centers[_idx]

    # Move one column in, to be sure of grid layout
    _d, _idx = ml_tree.query(ml_start + b)
    ml_start = ml_centers[_idx]

    # Initialize measurements
    alpha = []
    spacing_x = []
    spacing_y = []

    # Distance threshold for nearest neighbor search
    max_dist = 0.9 * spacing_guess // 2

    # #####################
    # Traverse horizontally
    # #####################

    # Find ML basis for vertical lines
    ml_curr = ml_start
    ml_basis = [ml_start]
    i = 0

    while True:
        # If on top row, move down using vector b
        if i % 2 == 0:
            _d, _idx = ml_tree.query(ml_curr + b)

        # Else, move up using a - b
        else:
            _d, _idx = ml_tree.query(ml_curr + a - b)

        # If below threshold, add to basis, otherwise we have left the grid
        if _d < max_dist:
            ml_curr = ml_centers[_idx]
            ml_basis.append(ml_curr)
            i += 1

        else:
            ml_basis = np.asarray(ml_basis)
            break

    # Iterate through ml_basis and perform vertical search
    for ml in ml_basis:
        i = 0
        ml_curr = ml
        ml_vert = [ml_curr]

        while True:
            # To get to next ML move to down/right, one left
            _d, _idx = ml_tree.query(ml_curr + 2*b - a)

            # If below threshold, add to basis, otherwise we have left the grid
            if _d < max_dist:
                ml_curr = ml_centers[_idx]
                ml_vert.append(ml_curr)
                i += 1

            else:
                ml_vert = np.asarray(ml_vert)
                break

        # Perform vertical line fit
        slope, off = np.polyfit(ml_vert[:, 0], ml_vert[:, 1], deg=1)

        alpha.append(np.arctan(slope))
        spacing_x.append(np.nanmean(np.diff(ml_vert[:, 0])))

        # Show result for debug
        if show:
            fig, ax = plt.subplots()
            im = ax.imshow(wi)
            ax.plot(ml_centers[:, 1], ml_centers[:, 0], 'o', color='black')
            ax.plot(ml_basis[:, 1], ml_basis[:, 0], 'o', color='red')
            ax.plot(ml_vert[:, 1], ml_vert[:, 0], 'o', color='blue')
            plt.show()

    # #####################
    # Traverse vertically
    # #####################

    # Find ML basis for vertical lines
    ml_curr = ml_start
    ml_basis = [ml_start]
    i = 0

    while True:
        # If in first colulmn, move right/down using b
        if i % 2 == 0:
            _d, _idx = ml_tree.query(ml_curr + b)

        # Else, move left using b - a
        else:
            _d, _idx = ml_tree.query(ml_curr - a + b)

        # If below threshold, add to basis, otherwise we have left the grid
        if _d < max_dist:
            ml_curr = ml_centers[_idx]
            ml_basis.append(ml_curr)
            i += 1

        else:
            ml_basis = np.asarray(ml_basis)
            break

    # Iterate through ml_basis and perform vertical search
    for ml in ml_basis:
        i = 0
        ml_curr = ml
        ml_horz = [ml_curr]

        while True:
            # To get to next ML move to right
            _d, _idx = ml_tree.query(ml_curr + a)

            # If below threshold, add to basis, otherwise we have left the grid
            if _d < max_dist:
                ml_curr = ml_centers[_idx]
                ml_horz.append(ml_curr)
                i += 1

            else:
                ml_horz = np.asarray(ml_horz)
                break

        # Perform vertical line fit (caution: coordinates are swapped for fit!)
        slope, off = np.polyfit(ml_horz[:, 1], ml_horz[:, 0], deg=1)

        alpha.append(- np.arctan(slope))
        spacing_y.append(np.nanmean(np.diff(ml_horz[:, 1])))

        # Show result for debug
        if show:
            fig, ax = plt.subplots()
            im = ax.imshow(wi)
            ax.plot(ml_centers[:, 1], ml_centers[:, 0], 'o', color='black')
            ax.plot(ml_basis[:, 1], ml_basis[:, 0], 'o', color='red')
            ax.plot(ml_horz[:, 1], ml_horz[:, 0], 'o', color='blue')
            plt.show()

    # Calculate final values from measurements
    alpha = np.nanmean(np.asarray(alpha))
    spacing_x = np.nanmean(spacing_x)
    spacing_y = np.nanmean(spacing_y)
    spacing = np.asarray([spacing_x, spacing_y])

    return spacing, float(alpha)


def est_grid_offset_dans(wi: ndarray,
                         ml_centers: ndarray,
                         spacing_est: ndarray,
                         rotation_est: float,
                         grid_type: str,
                         show: bool = False) -> float:
    """Estimate overall grid offset.

    Args:
        wi: White image

        show: Whether to show results for debugging.

    Returns:
        Estimated grid offset

    """

    # Get first, rough estimate without weighting the central region
    offset = est_offset(grid_type=grid_type, ml_centers=ml_centers, size=wi.shape,
                        spacing_est=spacing_est, rotation_est=rotation_est)

    return offset


def est_grid_params_cho_hex(wi: ndarray, spacing_guess: float, rot_guess: float):

    # Demosaic wi
    wi = rgb2gray(demosaic.get_demosaiced(wi))

    # Calculate ML centers
    logger.info("Calculating ML centers...")
    ml_centers = get_ml_centers_cho(wi=wi)
    logger.info("...done")

    # Crop ML centers, there are strong misdetections at image border
    ml_centers = crop_ml_centers(ml_centers=ml_centers,
                                 size=wi.shape,
                                 crop=int(10*spacing_guess))

    # plt.figure()
    # plt.imshow(wi)
    # plt.plot(ml_centers[:, 1], ml_centers[:, 0], 'x', color='red')
    # plt.show()

    # Calculate spacing and rotation using the ML centers
    # Spacing estimate is same as Dansereau
    spacing_est, _ = get_grid_params_dans_hex(
        ml_centers=ml_centers, spacing_guess=spacing_guess, rot_guess=rot_guess, wi=wi)

    rotation_est = get_grid_rotation_cho_hex(
        wi=wi, spacing_guess=spacing_guess, rot_guess=rot_guess)

    return spacing_est, rotation_est, ml_centers


def get_grid_rotation_cho_hex(wi, spacing_guess, rot_guess):
    im = wi.copy()
    n, m = im.shape

    # crop wi to uneven shape so that there is a central pixel
    if n % 2 == 0:
        im = im[:-1, :]
    if m % 2 == 0:
        im = im[:, :-1]
    n, m = im.shape

    # crop to square shape (window will crop it anyways)
    s = min(n, m)
    im_small = im[(n - s) // 2:s + (n - s) // 2, (m - s) // 2:s + (m - s) // 2]
    n, m = im_small.shape

    # First, calculate an estimate of the frequency vectors
    wi_fft = np.abs(images.fourier(im, shift=True, implementation='numpy'))

    peak_coords = get_fourier_peaks(wi_fft, constrained=True)

    # Find main frequency grid vectors
    # Only use peaks with positive f_x component and sort by descending f_y
    f_mat = peak_coords[0:6].copy()
    f_mat = f_mat[f_mat[:, 0] > 0, :]
    sort = np.argsort(f_mat[:, 1])
    f_mat = np.flipud(f_mat[sort, :])[0:2]

    rotation_est_1 = np.arctan(f_mat[0, 1] / f_mat[0, 0]) - np.deg2rad(60)
    rotation_est_2 = np.arctan(f_mat[1, 1] / f_mat[1, 0])

    return 0.5*(rotation_est_1 + rotation_est_2)


def est_grid_offset_cho(wi: ndarray,
                        ml_centers: ndarray,
                        spacing_est: ndarray,
                        rotation_est: float,
                        grid_type: str,
                        show: bool = False) -> float:
    """Estimate overall grid offset.

    Args:
        wi: White image

        show: Whether to show results for debugging.

    Returns:
        Estimated grid offset

    """

    # Get first, rough estimate without weighting the central region
    offset = est_offset(grid_type=grid_type, ml_centers=ml_centers, size=wi.shape,
                        spacing_est=spacing_est, rotation_est=rotation_est)

    return offset


def show(wi, centers_gt, centers_est):

    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, aspect='equal')

    ax.plot(centers_gt[:, 1], centers_gt[:, 0], 'o', color='blue', zorder=998)
    ax.plot(centers_est[:, 1], centers_est[:, 0], 'x', color='red', zorder=999)
    ax.set_aspect('equal', adjustable='box')

    cc = ax.imshow(wi, cmap='viridis', alpha=1.0)
    fig.colorbar(cc)

    plt.show()

    return
