# -*- coding: utf-8 -*-1.3
# Copyright (C) 2019  Maximilian Schambach
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Support functions for white image synthesis"""

from typing import List, Union, Optional

import numpy as np
from numpy.core.multiarray import ndarray
from plenpy.utilities import grids

__all__ = ['single_lenslet_vignetting', 'create_white_image']


def single_lenslet_vignetting(x_in: ndarray,
                              y_in: ndarray,
                              radius: Optional[float] = None) -> ndarray:
    """Calculate intensity from single lens vignetting.

    Args:
        x_in: Coordinates in x-direction.
        y_in: Coordinates in y-direction.
        radius: Radius of the lenslet in pixels. If ``None``,
        the radius for the Lytro Illum camera will be used.

    Returns:
        The intensity values from single lens vignetting as ndarray
        of shape (x_max, y_max).

    """
    x_max = x_in.shape[0]
    y_max = y_in.shape[0]
    x, y = np.meshgrid(x_in, y_in)
    z = np.zeros((x_max, y_max))

    # Lytro camera pixel pitch
    pixel_p = 1.399e-6

    if radius is None:
        # Use value of Lytro camera
        r = 0.5 * 1.999e-5
    else:
        r = pixel_p * radius

    # Lytro camera micro lens focal length
    f = 41.99e-6
    f_inv = 1.0 / f

    # Get coordinates that are less than r
    distance = (pixel_p * np.sqrt(x ** 2 + y ** 2)).T
    idx = np.where(distance <= r)

    t_plus = f_inv * (distance[idx] + r)
    t_minus = f_inv * (distance[idx] - r)

    z[idx] = t_plus / (t_plus ** 2 + 1) - t_minus / (t_minus ** 2 + 1) + np.arctan(
        t_plus) - np.arctan(t_minus)

    return z


def main_lens_vignetting(x_in: ndarray,
                         y_in: ndarray) -> ndarray:
    """Calculate intensity from main lens vignetting.

    Args:
        x: Coordinates in x-direction.
        y: Coordinates in y-direction.


    Returns:
        The intensity values from main lens vignetting as ndarray of shape (x_max, y_max).

    """
    x, y = np.meshgrid(x_in, y_in)

    # Lytro camera pixel pitch
    pixel_p = 1.399e-6

    r = x.shape[0] * 0.9e-6

    # Lytro camera micro lens focal length, fnumer is 2.11
    f = 2.11 * 2 * r
    f_inv = 1.0 / f

    distance = (pixel_p * np.sqrt(x ** 2 + y ** 2)).T
    t_plus = f_inv * (distance + r)
    t_minus = f_inv * (distance - r)

    z = t_plus / (t_plus ** 2 + 1) - t_minus / (t_minus ** 2 + 1) + np.arctan(
        t_plus) - np.arctan(t_minus)

    return z


def create_white_image(grid: grids.AbstractGrid) -> ndarray:
    """Calculate a white image from a grid.

    At every grid coordinate, the vignetting based intensity underneath the
    corresponding micro lens is calculated. The grid coordinate points
    can be arbitrarily shifted by sub pixel precision. Main lens vignetting
    is applied in an analogous fashion.

    The white image will be downscaled uint16.

    Args:
        grid: The grid object specifying the grid points.

    Returns:
        A 16 bit greyscale white image.
    """

    grid_coords = grid.gridPoints
    grid_parameters = grid.gridParameters

    size_x = grid_parameters.size[0]
    size_y =grid_parameters.size[1]

    x = np.arange(0, size_x)
    y = np.arange(0, size_y)

    white_image = np.zeros(grid_parameters.size)

    for point in grid_coords:
        white_image = np.maximum(
            single_lenslet_vignetting(
                x - point[0],
                y - point[1],
                radius=8),
            white_image)

    # Set Zero values to surrounding values
    mini = white_image[np.where(white_image != 0)].min()
    white_image[np.where(white_image == 0)] = mini

    # Shift to minimum zero
    white_image = white_image - np.min(white_image)

    # Rescale range to (max-min) and minimum to min to fit original white image
    white_image = (0.95 - 0.35) * (1/np.max(white_image)) * white_image + 0.35

    # Add main lens vignetting
    z = main_lens_vignetting(x - size_x/2, y - size_y/2)
    z /= z.max()
    white_image *= z

    # sample down to 16 bit and return
    return ((2**16 - 1)*white_image).astype(np.uint16)
