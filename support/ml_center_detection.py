# -*- coding: utf-8 -*-1.3
# Copyright (C) 2019  Maximilian Schambach
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Support functions for grid parameter estimation"""

from typing import Tuple
import logging as lg

import numpy as np
from numpy.core import ndarray

from scipy import ndimage
from skimage import filters as sk_filters
import scipy.ndimage.filters as sc_filters
import scipy.ndimage.filters as nd_filters
from scipy.ndimage.measurements import find_objects

from plenpy.utilities import kernels

# Logger set up
logger = lg.getLogger("estimate_grid")


def get_ml_centers_prop(wi: ndarray,
                        lam_est: float) -> Tuple[ndarray, ndarray]:
    """Estimate ml centers in only central part of the white image

    Args:
        wi: White image.
        lam_est: Estimated magnification factor lambda.

    Returns:
        Microlens center coordinates as ndarray of shape (N, 2).

    """
    x, y = wi.shape

    # Estimate region where deviation from perspectively to orthogonally
    # projected centers is less than 0.5px
    s_max = int(np.floor(0.5 / (lam_est - 1)))
    width = 2 * s_max - 1  # make odd width so center exists

    # Create Gauss window that matches the central window
    window = kernels.get_kernel_gauss(wi.shape[0], width/5,
                                      wi.shape[1], width/5,
                                      normalize='peak')

    # Create mask to crop image center
    mask = np.ones(wi.shape, dtype=bool)

    # Unmask center of the image
    start_x = (x - width) // 2
    start_y = (y - width) // 2
    mask[start_x:start_x + width, start_y:start_y + width] = 0

    wi_center = wi.copy()
    wi_center[mask] = 0.0

    # Convolve white image with gauss
    conv_kernel = kernels.get_kernel(name='gauss', size=5)

    conv_img = nd_filters.convolve(wi_center, conv_kernel)
    del wi_center

    # Gamma stretch
    conv_img = conv_img**5

    # Adaptive local thresholding to get local maximum regions
    adaptive_thresh = sk_filters.threshold_local(conv_img, 17)
    binary = conv_img > adaptive_thresh

    # Cut off all values outside the binary mask
    conv_img[~binary] = 0.0

    ml_clusters = np.ma.masked_array(conv_img, conv_img < 0.01)

    # Find clusters, which correspond the the microlenses, enumerate them
    cluster, num_of_clusters = ndimage.label(conv_img)
    idx = np.arange(1, num_of_clusters + 1)

    # Iterate over clusters, if too small or too large, delete
    # Get localization of each cluster
    loc = find_objects(cluster)

    # iterate through clusters label
    idx_new = []
    for i in idx - 1:

        # Get image values of cluster
        loc_tmp = loc[i]
        I_meas = cluster[loc_tmp]
        tmp = (I_meas == i + 1)
        N = tmp.nonzero()[0].size
        e = tmp.shape[0] / tmp.shape[1]

        # If cluster is too small or too large or too eccentric, ignore
        if 0.75 <= e <= 1.25 and 4*4 <= N <= 13*13:
            idx_new.append(i + 1)

    idx = idx_new
    # Calculate the subpixel microlens_coordinates by calculating the
    # center of mass of each detected cluster.
    ml_centers = np.asarray(
        ndimage.measurements.center_of_mass(
            ml_clusters,
            labels=cluster,
            index=idx))

    # import matplotlib.pyplot as plt
    # plt.figure()
    # plt.imshow(conv_img)
    # plt.plot(ml_centers[:, 1], ml_centers[:, 0], 'rx')
    # plt.show()

    return ml_centers, window


def get_ml_centers_cho(wi: ndarray) -> ndarray:
    """Method proposed by Cho et al.
    Based on clustering erosion and parabolic fitting.

    Args:
        wi: The white image used for estimate.

    Returns:
        Estimated ml center coordinates.

    """
    wi_eroded = wi.copy()

    # Adaptive local thresholding to get local maximum regions
    adaptive_thresh = sk_filters.threshold_local(wi_eroded, 17)
    binary = wi_eroded > adaptive_thresh

    # Cut off all values outside the binary mask
    wi_eroded[~binary] = 0.0
    wi_eroded = ndimage.grey_erosion(wi_eroded, [3, 3])

    # Find clusters, which correspond the the microlenses, enumerate them
    logger.info("Calculating ML clusters...")
    cluster, num_of_clusters = ndimage.label(wi_eroded)
    idx = np.arange(1, num_of_clusters + 1)
    logger.info("...done")

    # Get localization of each cluster
    loc = find_objects(cluster)

    # iterate through clusters label
    logger.info(f"Fitting lenses...")
    ml_centers = []
    for i in idx - 1:

        # Get image values of cluster
        loc_tmp = loc[i]
        I_meas = cluster[loc_tmp]
        tmp = I_meas.nonzero()
        N = tmp[0].size

        # Need at least 6  points for least squares fit
        if N >= 6:
            try:
                tmp = I_meas.nonzero()
                x_meas = loc_tmp[0].start + tmp[0]
                y_meas = loc_tmp[1].start + tmp[1]

                # Create signal model matrix
                phi = np.zeros((N, 5))
                phi[:, 0] = x_meas ** 2
                phi[:, 1] = x_meas
                phi[:, 2] = y_meas ** 2
                phi[:, 3] = y_meas
                phi[:, 4] = np.ones(N)

                # Calc parameters
                a = np.linalg.pinv(phi) @ wi_eroded[x_meas, y_meas]

                # Get paraboloid maximum
                x_max = -0.5 * a[1] / a[0]
                y_max = -0.5 * a[3] / a[2]

                # parameter b is x_max, d is y_max
                ml_centers.append(np.asarray([x_max, y_max]))

            except np.linalg.LinAlgError:
                logger.info("Optimal parameters not found. Skipping.")

    logger.info(f"...done")
    return np.asarray(ml_centers)


def get_ml_centers_dans(wi: ndarray) -> ndarray:
    """Calculat ML centers from WI,
    method proposed by Dansereau et al. in Matlab LightField Toolbox.

    Args:
        wi: White image used for estimate.

    Returns:
        Estimated ml center coordinates in shape (N, 2).

    """

    # Find local maxima of image
    data_max = sc_filters.maximum_filter(wi, 10)

    return np.asarray(np.where(wi == data_max)).T


def crop_ml_centers(ml_centers: ndarray, size: ndarray, crop: int):
    """Crop ml center coordinates by crop amount to get rid of
    edge defects"""

    cropped_centers = ml_centers.copy()

    # Crop the ML centers
    indices = []
    x_max, y_max = size

    for k in range(0, ml_centers.shape[0]):
        if ml_centers[k][0] < crop or \
                ml_centers[k][1] < crop or \
                ml_centers[k][0] > (x_max - 1 - crop) or \
                ml_centers[k][1] > (y_max - 1 - crop):
            indices.append(k)

    # delete elements that are too small
    cropped_centers = np.delete(cropped_centers, indices, axis=0)

    return cropped_centers
