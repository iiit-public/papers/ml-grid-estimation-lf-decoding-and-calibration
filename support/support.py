# -*- coding: utf-8 -*-1.3
# Copyright (C) 2019  Maximilian Schambach
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Support functions for create_scenes script."""

import itertools
from typing import Tuple, Optional
import uuid
import gc
import re

import numpy as np
from numpy.core import ndarray
import skimage.color
import colour_demosaicing as colour

from plenpy.utilities import kernels
import scipy.ndimage.filters as nd_filters


# Number of samples used in raytracer (has to be square)
NUM_SAMPLES_LENS = 19881
NUM_SAMPLES_MICROLENS = 100

# Camera Parameters (as Lytro ILLUM)
SENSORSIZE = [7728, 5368]
FNUM = 2.0
FNUM_ML = 0.975 * FNUM  # Factor, see IIIT-Raytracer
MICROLENSRADIUS = 10e-6
PIXELPITCH = 1.4e-6

MLATYPE = "Hex" # Either "Hex" or "Rect"


def wi_preprocess(wi: ndarray, noise: Optional[float] = None) -> ndarray:
    """Preprocessing of raw white image.

    Args:
        wi: White image as 16bit RGB image.

        noise: Standard deviation of image noise to add.

    Returns:
        Preprocessed white image.
    """
    # Resample to 10 bit (stored in 16 bit uint) and convert to float value
    res = ((((2**10 - 1) * (wi.astype(np.float64) / (2**16 - 1))).astype(np.uint16)) / (2**10 - 1)).astype(np.float32)

    # Gamma according to lytro metadata
    res = res ** 0.4

    # Apply RGB response according to Lytro's metadata
    res = res * [0.7761, 1.0, 0.7345]

    # Mosaic
    res = colour.mosaicing_CFA_Bayer(res, pattern='GRBG')

    # Add noise
    if not noise == 0:
        noise_mean = 0
        noise_sigma = noise

        gauss_ = np.random.normal(noise_mean, noise_sigma, res.shape).astype(
            np.float32)
        res += gauss_
        del gauss_
        gc.collect()

    # Convert to Greyscale
    res = skimage.color.rgb2gray(res)

    # Contrast stretch
    res -= res.min()
    res /= res[2500:-2500, 3500:-3500].max()
    res = np.clip(res, 0, 1)

    # Gamma correct
    res = res ** (1 / 0.4)

    return res


def get_lens_radius(imageDistance: float, focusDistance: float):

    # Calculate remaining
    lensFocalLength = 1.0 / (1.0 / imageDistance + 1.0 / focusDistance)
    lensRadius = lensFocalLength / 2.0 / FNUM

    return lensRadius


def get_projected_centers(centers, imageDistance: float):

    # Micro lens focal length
    microLensFocalLength = FNUM_ML * 2.0 * MICROLENSRADIUS

    # Scaling factor (central projection)
    sfac = (imageDistance + microLensFocalLength) / imageDistance

    # project centers and convert to pixels
    centers_p = (sfac * centers[:, :2] / PIXELPITCH) + 0.5 * np.asarray(SENSORSIZE)

    # flip image coordinates (as is done in raytracer)
    centers_p = np.asarray(SENSORSIZE) - centers_p - 1

    # Shift as pixel starts at -0.5, -0.5 ant not as 0, 0 as in raytracer
    centers_p += [0.5, 0.5]

    return np.fliplr(centers_p)


def get_orthogonal_centers(centers, imageDistance: float):

    # project centers and convert to pixels
    centers_o = (centers[:, :2] / PIXELPITCH) + 0.5 * np.asarray(SENSORSIZE)

    # flip image coordinates (as is done in raytracer)
    centers_o = np.asarray(SENSORSIZE) - centers_o - 1

    # Shift as pixel starts at -0.5, -0.5 ant not as 0, 0 as in raytracer
    centers_o += [0.5, 0.5]

    return np.fliplr(centers_o)


def get_json(lens_radius, image_distance, focus_distance,
             offset, rotation, grid_noise):

    res = {}

    # Geometry of scene
    res['geometry'] = dict()
    # objects
    res['geometry']['objects'] = [dict()]
    res['geometry']['objects'][0]["type"] = "Sphere"
    res['geometry']['objects'][0]["radius"] = 0.01
    res['geometry']['objects'][0]["position"] = [0.0, -10.0, 0.0]
    res['geometry']['objects'][0]["material"] = dict()
    res['geometry']['objects'][0]["material"]["type"] = "Matte"
    res['geometry']['objects'][0]["material"]["kd"] = 1.0
    res['geometry']['objects'][0]["material"]["texture"] = dict()
    res['geometry']['objects'][0]["material"]["texture"]["type"] = "ConstantColor"
    res['geometry']['objects'][0]["material"]["texture"]["color"] = [255, 255, 255]
    # backgroundcolor
    res['geometry']['backgroundColor'] = [255, 255, 255]

    # Scene lights
    res['lights'] = [dict()]
    res['lights'][0]["type"] = "AmbientLight"
    res['lights'][0]["color"] = [255, 255, 255]
    res['lights'][0]["radianceScaling"] = 1.0

    # Scene Camera
    res['camera'] = dict()
    res['camera']["type"] = "LightFieldCameraCalibration"
    res['camera']["imageType"] = "IdealRgb"
    res['camera']["eye"] = [0.0, -2.0, 0.45]
    res['camera']["lookAt"] = [0.0, 0.9, 0.85]
    res['camera']["up"] = [0.0, 0.0, 1.0]

    res['camera']["lensRadius"] = lens_radius
    res['camera']["microLensRadius"] = MICROLENSRADIUS
    res['camera']["imageDistance"] = image_distance
    res['camera']["focusDistance"] = focus_distance

    res["camera"]["grid"] = dict()
    res["camera"]["grid"]["type"] = MLATYPE
    res['camera']['grid']['offset'] = list(offset)
    res['camera']['grid']['rotation'] = rotation
    res['camera']['grid']['noise'] = grid_noise
    res['camera']['grid']['save'] = True

    # main lens sampler
    res["camera"]["sampler"] = dict()
    res["camera"]["sampler"]["type"] = "MultiJittered"
    res["camera"]["sampler"]["numSamples"] = NUM_SAMPLES_LENS

    res["camera"]["viewPlane"] = dict()
    res["camera"]["viewPlane"]["resolution"] = SENSORSIZE
    res["camera"]["viewPlane"]["pixelSize"] = PIXELPITCH
    res["camera"]["viewPlane"]["gamma"] = 1.0

    # micro lens sampler
    res["camera"]["viewPlane"]["sampler"] = dict()
    res["camera"]["viewPlane"]["sampler"]["type"] = "MultiJittered"
    res["camera"]["viewPlane"]["sampler"]["numSamples"] = NUM_SAMPLES_MICROLENS

    # Spectrum Type
    res["spectrumType"] = "Monochromatic"

    return res


def add_vignetting(json: dict, image_distance: float, vignetting: str):
    """Adds aperture for michanical vignetting. The values are hand crafted.

    Args:
        json:
        image_distance:
        vignetting:
    """

    if not vignetting == 0:

        json["camera"]["apertures"] = [dict()]
        json["camera"]["apertures"][0]["type"] = "CircularAperture"

        if image_distance == 0.03:

            if vignetting == 1:
                json["camera"]["apertures"][0]["aperturePosition"] = 0.07
                json["camera"]["apertures"][0]["radius"] = 0.019
            elif vignetting == 2:
                json["camera"]["apertures"][0]["aperturePosition"] = 0.08
                json["camera"]["apertures"][0]["radius"] = 0.018
            else:
                raise ValueError(f"Unknown vignetting {vignetting}.")

        elif image_distance == 0.047:

            if vignetting == 1:
                json["camera"]["apertures"][0]["aperturePosition"] = 0.78
                json["camera"]["apertures"][0]["radius"] = 0.111
            elif vignetting == 2:
                json["camera"]["apertures"][0]["aperturePosition"] = 0.8
                json["camera"]["apertures"][0]["radius"] = 0.11
            else:
                raise ValueError(f"Unknown vignetting {vignetting}.")

        elif image_distance == 0.117:
            if vignetting == 1:
                json["camera"]["apertures"][0]["aperturePosition"] = 0.85
                json["camera"]["apertures"][0]["radius"] = 0.06

            elif vignetting == 2:
                json["camera"]["apertures"][0]["aperturePosition"] = 0.9
                json["camera"]["apertures"][0]["radius"] = 0.059
            else:
                raise ValueError(f"Unknown vignetting {vignetting}.")

        else:
            raise ValueError("Unknown image distance for vignetting. Please add vignetting for this specific image distance.")

    return


def get_filename(image_distance, rotation, offset, grid_noise, vignetting):

    res = "imageDistance-" + str(image_distance) + "_"
    res += "rotation-" + str(rotation) + "_"
    res += "offset-[" + str(round(offset[0], 8)) + "," + str(round(offset[1], 8)) + "]_"
    res += "grid_noise-" + str(grid_noise) + "_"
    res += "vignetting-" + str(vignetting) + "_"
    res += "_ID-" + str(uuid.uuid4())[:8]
    return res


def expand_metadata(metadata):

    sensor_size = np.asarray(metadata['sensorSize'])
    pixel_pitch = metadata['pixelPitch']

    image_distance = metadata['imageDistance']
    focus_distance = metadata['focusDistance']
    lens_radius = metadata['lensRadius']
    microlens_radius = metadata['microlensRadius']
    lens_focal_length = 1.0 / (1.0 / image_distance + 1.0 / focus_distance)
    f_num = metadata['fNumber']
    f_num_ml = FNUM_ML
    ml_focal_length = f_num_ml * 2.0 * microlens_radius

    grid_type = metadata['gridType']
    rotation = metadata['rotation']
    offset = metadata['offset']
    grid_noise = metadata['grid_noise']

    vignetting = metadata['vignetting']

    json_filename = metadata['raytracerJsonfile']
    id = re.match(r'(.*)__ID-(.*).json', json_filename).group(2)

    return id, sensor_size, pixel_pitch, image_distance, focus_distance, \
           lens_radius, microlens_radius, lens_focal_length, ml_focal_length,\
           f_num, f_num_ml, grid_type, rotation, offset, grid_noise, vignetting