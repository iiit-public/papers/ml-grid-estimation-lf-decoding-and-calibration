% -*- coding: utf-8 -*-1.3
% Copyright (C) 2019  Maximilian Schambach
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

syms alpha beta gamma delta w h sx sy z;
syms p f d F

% Rotation matrices
syms Rx(angle) Ry(angle) Rz(angle);
Rx(angle) = [1 0 0; 0 cos(angle) -sin(angle); 0 sin(angle) cos(angle)];
Ry(angle) = [cos(angle) 0 sin(angle); 0 1 0; -sin(angle) 0 cos(angle)];
Rz(angle) = [cos(angle) -sin(angle) 0; sin(angle) cos(angle) 0; 0 0 1];

% MLA edge vector 
xvec = [w/2; h/2; 0];

% calculate w, h in terms of s_x, s_y
tmp = Rx(gamma) * Ry(beta) * Rz(alpha) * xvec;
eq1 = tmp(1) == sx / 2;
eq2 = tmp(2) == sy / 2;

res = solve([eq1, eq2], [w, h]);
syms w_calc(alpha, beta, gamma) h_calc(alpha, beta, gamma);
w_calc(alpha, beta, gamma) = res.w;
h_calc(alpha, beta, gamma) = res.h;



% calculate maximum tilt using same angle in all rotations
tmp = Rx(gamma) * Ry(-beta) * Rz(alpha) * [w_calc(alpha, -beta, gamma); h_calc(alpha, -beta, gamma); 0];
disp('Rotation equal in all axes:')
res1 = simplify(tmp(3))

%  z rotation equal zero
tmp = Rx(gamma) * Ry(-beta) * Rz(0) * [w_calc(0, -beta, gamma); h_calc(0, -beta, gamma); 0];
disp('No rotation aroung z-axis:')
res2(beta, gamma) = simplify(tmp(3))

% Taylor expansion in delta = 0
disp('Taylor expansion in delta = 0 :')
g(beta, gamma) = taylor(res2(beta, gamma), [beta, gamma], 'Order', 4)

disp('Taylor expansion for beta = gamma  == delta:')
g(delta, delta)
