# -*- coding: utf-8 -*-1.3
# Copyright (C) 2019  Maximilian Schambach
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from scipy.optimize import root_scalar

F = 0.03    # 30 mm main lens
fNum = 2    # f-number of system

# fNum_ml = 0.975*fNum    # A little smaller for synthetic images (see raytracer)
fNum_ml = fNum

d = 20e-6   # ML diameter
p = 1.4e-6  # pixel pitch
size = [7728, 5368]  # sensor size
size = np.asarray(size)
sx, sy = size

# Calculate remaining camera parameters
f = fNum_ml * d

# Get grid spacing
dx = d
dy = np.sqrt(3)*dx / 2


########################
#### DEFINITIONS
########################

def delta_tilt(delta):
    """Calculate maximum tilt delta"""
    global sx, sy, p, f, d
    return delta*sx*p + delta*sy*p + (5 * delta**3 * sx*p)/6 + (delta**3 * p*sy)/3 - p*f/d


# Define rotation matrices
def Rx(angle):
    return np.asarray([[1, 0, 0],
                       [0, np.cos(angle), -np.sin(angle)],
                       [0, np.sin(angle), np.cos(angle)]])


def Ry(angle):
    return np.asarray([[np.cos(angle),  0, np.sin(angle)],
                       [0,              1, 0],
                       [-np.sin(angle), 0, np.cos(angle)]])


def Rz(angle):
    return np.asarray([[np.cos(angle), -np.sin(angle), 0],
                       [np.sin(angle), np.cos(angle),  0],
                       [0,             0,              1]])


def c_ideal(i, j):
    """Ideal grid point coordinate at (i,j) for hexagonal grid"""

    global dx, dy

    x_ = dx * (i + 0.5 * (j % 2))
    y_ = dy * j
    z_ = 0

    return np.asarray([x_, y_, z_])


def c(i, j, alpha=0, beta=0, gamma=0):
    """Grid point coordinate at (i,j) for hexagonal grid"""

    global F

    return Rx(gamma) @ Ry(beta) @ Rz(alpha) @ c_ideal(i, j) + np.asarray([0, 0, -F])


def lambda_fact(i, j, alpha=0, beta=0, gamma=0):
    """Projection factor for the perspectively projected centers"""

    global F, f
    tmp = Rx(gamma) @ Ry(beta) @ Rz(alpha) @ c_ideal(i, j)

    return (-F - f) / (tmp[2] - F)


def c_p(i, j, alpha=0, beta=0, gamma=0):
    """Perspectively projected grid center at (i, j) and z=-F-f"""

    return lambda_fact(i, j, alpha, beta, gamma)*c(i, j, alpha, beta, gamma)


def dx_dist(i, j, alpha=0, beta=0, gamma=0):
    """Local grid spacing in x-direction of perspectively projected centers"""

    return np.linalg.norm(c_p(i, j, alpha, beta, gamma) - c_p(i - 1, j, alpha, beta, gamma))


def dy_dist(i, j, alpha=0, beta=0, gamma=0):
    """Local grid spacing in y-direction of perspectively projected centers"""

    return 0.5*np.sqrt(3)*np.linalg.norm(c_p(i, j, alpha, beta, gamma) - c_p(i, j - 1, alpha, beta, gamma))


########################
#### CALCULATIONS
########################

# Get grid tilt upper bounds
res = root_scalar(delta_tilt, x0=0, x1=0.01)
delta_max = res.root

i_max = np.ceil(p*sx / (2*dx))
j_max = np.ceil(p*sy / (2*dy))

alpha = 0
beta = -delta_max   # clockwise
gamma = delta_max   # counter clockwise

Ddx = np.abs(dx_dist(i_max, j_max, alpha, beta, gamma) - dx_dist(-i_max, -j_max, alpha, beta, gamma))
Ddy = np.abs(dy_dist(i_max, j_max, alpha, beta, gamma) - dy_dist(-i_max, -j_max, alpha, beta, gamma))
Dd = np.hypot(Ddx, Ddy)

print("Maximum distortion in white images (px):")
print(Dd)
