# -*- coding: utf-8 -*-1.3
# Copyright (C) 2019  Maximilian Schambach
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Script to create json scenes for specified optimal camera parameters.

import argparse
import datetime
import itertools
import json
import os

import numpy as np

from support.support import get_lens_radius, get_json, get_filename, add_vignetting
from support.support import PIXELPITCH, SENSORSIZE, MICROLENSRADIUS, MLATYPE, \
    NUM_SAMPLES_LENS, NUM_SAMPLES_MICROLENS, FNUM

# Argument setup
parser = argparse.ArgumentParser(
    description="Create JSON scene files for a camera.")

parser.add_argument('-i', '--imagedistance', type=float,
                    default=[0.03, 0.047, 0.117],
                    nargs='+', help='Image Distance (focal length at inf)')

parser.add_argument('-f', '--focusdistance', type=float,
                    default=[10000.0],
                    nargs='+', help='Distance of focal plane')

parser.add_argument('-r', '--rotation', type=float,
                    default=[0, 0.001, 0.005, -0.001, -0.005],
                    nargs='+', help='Rotation of grid')

parser.add_argument('-o', '--offset', type=float,
                    default=[[0.201*PIXELPITCH, 0.337*PIXELPITCH],
                             [0.781*PIXELPITCH, 0.103*PIXELPITCH]],
                    nargs='+', help='Offset of grid')

parser.add_argument('-g', '--gridnoise', type=float,
                    default=[0, 0.1],
                    nargs='+', help='Noise of grid points (standard deviation in percent of grid spacing)')

parser.add_argument('-v', '--vignetting', type=int,
                    default=[0, 1, 2],
                    nargs='+', help='Degree of vignetting, 0: none, 1: little, 2: strong')

parser.add_argument('--output', metavar='PATH',
                    default="./Scenes",
                    help='Output folder path.')


args = parser.parse_args()

# Extract arguments
image_distances = np.asarray(args.imagedistance)
focus_distances = np.asarray(args.focusdistance)
offsets = np.asarray([args.offset])
if offsets.ndim == 3:
    offsets = np.squeeze(offsets)
rotations = np.asarray(args.rotation)
grid_noises = np.asarray(args.gridnoise)
vignettings = np.asarray(args.vignetting)


# System paths
folder_out = os.path.abspath(args.output)

# Create subfolder if non existent
if not os.path.exists(folder_out):
    os.makedirs(folder_out)

# Iterate over all possible parameter combinations
counter = 0
for image_distance, focus_distance, offset, rotation, grid_noise, vignetting in itertools.product(
        image_distances, focus_distances, offsets, rotations, grid_noises, vignettings):

    # Calculate missing camera parameters in dist_range
    lens_radius = get_lens_radius(image_distance, focus_distance)

    # Get raytracer scene json
    full_json = get_json(lens_radius, image_distance, focus_distance,
             offset, rotation, grid_noise)

    add_vignetting(full_json, image_distance, vignetting)

    json_str = json.dumps(full_json, indent=4)

    # Write file to disk
    filename = get_filename(image_distance, rotation, offset, grid_noise, vignetting)
    file = open(os.path.join(folder_out, filename + ".json"), 'w')

    # Write JSON file to disk
    file.write(json_str)

    # Close the file
    file.close()

    # Save the Metadata in JSON format
    meta_dict = {
        "date": str(datetime.datetime.now()),
        "raytracerJsonfile": filename + ".json",
        "sensorSize": SENSORSIZE,
        "pixelPitch": PIXELPITCH,
        "imageDistance": image_distance,
        "focusDistance": focus_distance,
        "lensRadius": lens_radius,
        "microlensRadius": MICROLENSRADIUS,
        "fNumber": FNUM,
        "gridType": MLATYPE,
        "rotation": rotation,
        "offset": list(offset),
        "grid_noise": grid_noise,
        "numSamplesMainLens": NUM_SAMPLES_LENS,
        "numSamplesMicroLens": NUM_SAMPLES_MICROLENS,
        "vignetting": int(vignetting)
    }

    json.dump(
        meta_dict,
        open(os.path.join(folder_out, filename + ".metadata"), 'w'),
        indent=4)
